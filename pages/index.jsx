import { MongoClient } from "mongodb";
import Head from "next/head";
import { Fragment } from "react";

import MeetupList from "../components/meetups/MeetupList";

const HomePage = (props) => {
  return (
    <Fragment>
      <Head>
        <title>Makanan Dwiky</title>
        <meta
          name="description"
          content="Berbagai macam makanan si dwiky, termasuk yang makanan kotoran sendiri karena kelaparan."
        />
      </Head>
      <MeetupList meetups={props.meetups} />;
    </Fragment>
  );
};

// Pake yg ini kalo isinya sering berubah2
// export async function getServerSideProps(context) {
//   const req = context.req;
//   const res = context.res;

//   // fetch data from api

//   return {
//     props: {
//       meetups: DUMMY_MEETUPS
//     }
//   }
// }

// Kalo isinya g berubah2, pake ini, lebih cepet juga
export async function getStaticProps() {
  // fetch data from api di sini aja, karena getStaticProps itu g akan muncul di client side
  const client = await MongoClient.connect(
    "mongodb://127.0.0.1:27017/meetup-database"
  );
  const db = client.db();

  const meetupsCollection = db.collection("meetups");

  const response = await meetupsCollection.find().toArray();

  const transformedResponse = response.map((meetup) => ({
    id: meetup._id.toString(),
    title: meetup.title,
    address: meetup.address,
    image: meetup.image,
  }));

  client.close();

  return {
    props: {
      meetups: transformedResponse,
    },
    revalidate: 3600,
  };
}

export default HomePage;
