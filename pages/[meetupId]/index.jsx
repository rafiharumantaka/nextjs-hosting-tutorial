import { MongoClient, ObjectId } from "mongodb";
import { Fragment } from "react";
import Head from 'next/head'

import MeetupDetail from "../../components/meetups/MeetupDetail";

const MeetUpDetailPage = (props) => {
  return (
    <Fragment>
      <Head>
        <title>RM | props.meetupData.title</title>
        <meta
          name="description"
          content={props.meetupData.description}
        />
      </Head>
      <MeetupDetail
        image={props.meetupData.image}
        title={props.meetupData.title}
        address={props.meetupData.address}
        description={props.meetupData.description}
      />
    </Fragment>
  );
};

export async function getStaticPaths() {
  const client = await MongoClient.connect(
    "mongodb://127.0.0.1:27017/meetup-database"
  );
  const db = client.db();

  const meetupsCollection = db.collection("meetups");

  const meetups = await meetupsCollection.find({}, { _id: 1 }).toArray();

  client.close();

  return {
    paths: meetups.map((meetup) => ({
      params: { meetupId: meetup._id.toString() },
    })),
    fallback: true,
  };
}

export async function getStaticProps(context) {
  // fetch api
  const meetupId = context.params.meetupId; // butuh getStaticPatch buat dapetin idnya
  const client = await MongoClient.connect(
    "mongodb://127.0.0.1:27017/meetup-database"
  );
  const db = client.db();

  const meetupsCollection = db.collection("meetups");

  const meetupData = await meetupsCollection.findOne({ _id: ObjectId(meetupId) });

  const transformedMeetupData = {
    id: meetupData._id.toString(),
    title: meetupData.title,
    image: meetupData.image,
    description: meetupData.description,
    address: meetupData.address
  }

  client.close();

  return {
    props: {
      meetupData: transformedMeetupData,
    },
  };
}

export default MeetUpDetailPage;
