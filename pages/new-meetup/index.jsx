import {useRouter} from 'next/router'
import { Fragment } from 'react'
import Head from 'next/head'

import NewMeetupForm from "../../components/meetups/NewMeetupForm"

const NewMeetUpPage = () => {
  const router = useRouter()

  const addMeetupHandler = async (meetupData) => {
    const response = await fetch('/api/new-meetup', {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(meetupData)
    })

    const responseData = await response.json()

    console.log(responseData)
    router.push('/')
  }

  return (
    <Fragment>
      <Head>
        <title>Tambah Makanan Dwiky</title>
        <meta
          name="description"
          content="Form untuk menambah asupan gizi dwikkeh."
        />
      </Head>
      <NewMeetupForm onAddMeetup={addMeetupHandler} />
    </Fragment>
  )
}

export default NewMeetUpPage